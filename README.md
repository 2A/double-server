# Server

OpenWebRTC signaling / chat server for controlling a [Double](http://www.doublerobotics.com/)  robot

Forked from [https://github.com/EricssonResearch/openwebrtc-examples/blob/master/web](https://github.com/EricssonResearch/openwebrtc-examples/tree/master/web).

Should work together with [iBrains](https://bitbucket.org/2A/double-ibrains) running in Double's iPad and [DroidMote](https://bitbucket.org/2A/double-droidmote) Android-app as teleoperation client.

## Starting the server
The server uses [node.js](http://nodejs.org) and is started using:
```
node server.js
```
The default port is 8080 (and 8443 for SSL). The port to use can be changed by setting the environment variables PORT and HTTPS_PORT or giving the port as an argument to the node command. If both the environment variable and the argument are given then the argument is used.

Example of how to set port using environment variable and command line argument.
```
PORT=9080 node server.js
node server.js 10080
```

Logs for command/status/ping messages are written under `logs/` folder. 

## Local testing
The simple WebRTC app is now running at [http://localhost:8080/](http://localhost:8080/)
